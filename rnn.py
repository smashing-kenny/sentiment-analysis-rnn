import pandas

from keras.preprocessing import sequence
from keras.models import load_model
from keras.layers import Dense, Activation, Embedding
from keras.layers import LSTM, SpatialDropout1D, Bidirectional

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
#from sklearn.model_selection import train_test_split
import sys

print("Number of arguments:{} List:{}".format(len(sys.argv), str(sys.argv)))

model = load_model(sys.argv[1])
tokenizer = Tokenizer(num_words=10000)


submission = pandas.read_csv('data/sampleSubmission.csv')
phrases = pandas.read_csv('data/test.tsv',  sep="\t").Phrase.apply(lambda x: x.lower())

phrases = tokenizer.texts_to_sequences(phrases)
phrases = pad_sequences(phrases, maxlen=60)

submission['Sentiment2'] = model.predict_classes(phrases, batch_size=64, verbose=2)
submission.to_csv('rnn_result.csv', index=False)
