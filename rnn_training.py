import numpy as np
import pandas

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding
from keras.layers import LSTM, SpatialDropout1D, Bidirectional

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from pprint import pprint
import sys
import datetime

def prepare_train_data(train_tsv):
	train_ds = pandas.read_csv(train_tsv,  sep="\t")

	train_ds = train_ds \
				.sample(frac=1) \
				.reset_index(drop=True)

	X = train_ds.Phrase.apply(lambda x: x.lower())
	Y = to_categorical(train_ds.Sentiment.values)

	tokenizer = Tokenizer(num_words=10000)
	tokenizer.fit_on_texts(list(X))

	X = tokenizer.texts_to_sequences(X)
	X = pad_sequences(X, maxlen=60)


	x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, random_state=42)

	return (x_train, y_train), (x_test, y_test)



np.random.seed(42)

(x_train, y_train), (x_test, y_test) = prepare_train_data(train_tsv='data/train.tsv')

model = Sequential()
model.add(Embedding(10000, 32))
model.add(SpatialDropout1D(0.2))
model.add(Bidirectional(LSTM(100, dropout=0.2, recurrent_dropout=0.2)))  #Accuracy on test data: 86.85%

model.add(Dense(5, activation="sigmoid"))

model.compile(loss='binary_crossentropy',
			  optimizer='adam',
			  metrics=['accuracy'])

model.summary()


model.fit(x_train,
		  y_train,
		  batch_size=64,
		  epochs=7,
		  validation_data = (x_test, y_test),
		  verbose=2)

scores = model.evaluate(x_test,
						y_test,
						batch_size=64)

print("Accuracy on test data: {:.2f}".format(scores[1] * 100))

model.save("rnn-{:%Y-%m-%d %H-%M}-{:.2f}.h5".format(datetime.datetime.now(), (scores[1] * 100)))


