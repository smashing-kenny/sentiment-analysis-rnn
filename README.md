# Sentiment Analysis RNN

##You will need

1. Python 3
2. Keras
3. TensorFlow
4. Theano
5. Pandas
6. Scikit-learn

##How to run

1. For training rnn_training.py
2. For using rnn.py [MODEL] for example rnn.py model.h5


##Model summary
Layer (type) | Output Shape | Param #   
------------- | ------------- | -------------
embedding_1 (Embedding)  | (None, None, 32)  | 320000
spatial_dropout1d_1  | (Spatial (None, None, 32) | 0
bidirectional_1  | (Bidirection (None, 200) | 106400
dense_1 (Dense)  | (None, 5) | 1005

1. Total params: 427,405
2. Trainable params: 427,405
3. Non-trainable params: 0
4. Accuracy on test data: 86.85%